#define		SET_SIZE		1024

struct hash_table_node {
	hash_table_node *next = nullptr;
	char *key = nullptr;
	int value;

	hash_table_node(char *_key, int _value) {
		next = nullptr;
		key = _key;
		value = _value;
	}
};

struct hash_table {
	hash_table_node *set[SET_SIZE] = {nullptr};

	void add(char *key, int value) {
		hash_table_node *current = set[hash_code(key) % SET_SIZE];
		if(current == nullptr) {
			set[hash_code(key) % SET_SIZE] = new hash_table_node(key, value);
		} else {
			while(current->next != nullptr) {
				current = current->next;
			}

			current->next = new hash_table_node(key, value);
		}
	}

	int get(char *key) {
		hash_table_node *current = set[hash_code(key) % SET_SIZE];
		if(current == nullptr) {
			return -1;
		} else {
			while(current != nullptr) {
				if(strcmp(current->key, key) == 0) {
					return current->value;
				}

				current = current->next;
			}

			return -1;
		}
	}

	unsigned long hash_code(char *str) {
		unsigned long hash = 5381;
		int c;

		while(c = (*str++)) {
			hash = ((hash << 5) + hash) + c;
		}

		return hash;
	}
};