void read_file(const char *filename, char *&fileBuffer) {
	// Open the file
	FILE *file = fopen(filename, "r");
	if(file == nullptr) {
		error_exit("Could not open file.");
	}

	// Get the fileSize
	fseek(file, 0, SEEK_END);
	long fileSize = ftell(file);
	rewind(file);

	// Put the file into memory
	fileBuffer = (char *) malloc(sizeof(char) *fileSize);
	if(fileBuffer == nullptr) {
		error_exit("Memory allocation failed.");
	}

	// Check for differences between file size and buffer size
	if(fileSize != fread(fileBuffer, 1, fileSize, file)) {
		error_exit("Dictionary size and buffer size differ.");
	}

	// Close the file
	fclose(file);
}

unsigned int file_size(FILE *file) {
	fseek(file, 0, SEEK_END);
	unsigned int size = ftell(file);
	rewind(file);

	return size;
}