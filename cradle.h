#include <iostream>
#include "symbols.h"
#include "table.h"

#define expression_term(type) type == TK_ID_INT || type == TK_NUMBER || math_operator(type) || type == TK_LPARENT || type == TK_RPARENT || type == TK_COMMA || type == TK_ID_LONG
#define math_operator(type) type == TK_SUM || type == TK_DIVIS || type == TK_MULT || type == TK_MODULO
#define identifier_or_number(type) type == TK_ID_INT || type == TK_NUMBER || type == TK_ID_LONG
#define comparator(type) type == TK_GREATER || type == TK_LESSER || type == TK_GREATEREQ || type == TK_LESSEREQ || type == TK_SAME || type == TK_DIFFERENT

void log(const char *message) {
	std::cout << message;
}

void logln(const char *message) {
	std::cout << message << '\n';
}

void error_log(const char *message) {
	std::cout << "Error: " << message << '\n';
}

void error_exit(const char *message) {
	std::cout.clear();
	error_log(message);
	exit(1);
}

void exit() {
	exit(0);
}