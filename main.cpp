#define		VERSION			"1.0.1"

#include "cradle.h"
#include "list.h"
#include "token.h"
#include "keywords.h"
#include "hash_table.h"
#include "file_manager.h"
#include "scope.h"
#include "writer.h"
#include "parser.h"
#include "lexer.h"
#include "flags.h"

int main(int x, const char* flags[]) {
	std::cout.setstate(std::ios_base::failbit);
	handle_flags(x, flags);

	std::cout << "\n--- Start of compilation ---\n\n";
	std::cout << "Input name: " << file_name << "\n";
	std::cout << "Output name: " << output_name << "\n\n";

	char *file_buffer = nullptr;
	read_file(file_name, file_buffer);

	std::cout << "\t - LEXING -\n";
	List<Token> tokens(10000);
	reserve_keywords();
	lexer(file_buffer, tokens);
	std::cout << tokens << '\n';

	write_header();
	write_output_routines();
	write_start_of_text();

	std::cout << "\t - PARSING -\n";
	parse(tokens);

	write_exit();
	set_addresses();
	change_output_permissions();
	std::cout << "\n--- End of compilation ---\n\n";
	return 0;
}
