int precedence(int type) {
	if(type == TK_MULT || type == TK_DIVIS || type == TK_MODULO)  {
		return 1;
	}

	return 0;
}

void shunting_yard(List<Token> &expression) {
	int maximum = expression.index;
	List<Token> stack(maximum);
	expression.index = 0;

	for(int i = 0; i < maximum; i++) {
		Token current = expression.get(i);
		if(identifier_or_number(current.type)) {
			expression.add(current);
		}
		else if(current.type == TK_FUNCTION) {

		}
		else if(math_operator(current.type)) {
			while(!stack.empty() && (math_operator(stack.top().type)) && precedence(current.type) <= precedence(stack.top().type)) {
				expression.add(stack.pop());
			}

			stack.add(current);
		}
		else if(current.type == TK_COMMA) {
			while(stack.top().type != TK_LPARENT) {
				if(stack.empty()) {
					error_exit("Either a separator is misplaced or a parenthesis is missmatched");
				}

				expression.add(stack.pop());
			}
		}
		else if(current.type == TK_LPARENT) {
			stack.add(current);
		}
		else if(current.type == TK_RPARENT) {
			while(stack.top().type != TK_LPARENT) {
				if(stack.empty()) {
					error_exit("Missmatched parenthesis.");
				}

				expression.add(stack.pop());
			}

			stack.pop();
		}
		else {
			error_exit("Wrong syntax.");
		}
	}

	while(!stack.empty()) {
		if(stack.top().type == TK_LPARENT || stack.top().type == TK_RPARENT) {
			error_exit("Missmatched parenthesis.");
		}

		expression.add(stack.pop());
	}
}

void parse(List<Token> &tokens) {
	List<Token> expression(1000);
	int offset = 0;

	for(int i = 0; i < tokens.index; i++) {
		Token current = tokens.get(i);
		switch(current.type) {
			case TK_INT: {
				logln("Variable declaration ->");

				if(tokens.get(++i).type != TK_ID_INT) {
					error_exit("Identifier expected.");
				}

				Token assignee = tokens.get(i);
				if(symbol_table.get(assignee.value) != -1) {
					error_exit("Identifier already used.");
				}
				else {
					offset += 4;
					symbol_table.add(assignee.value, offset);
				}
				std::cout << "Assignee: " << assignee.value << "\tPosition: " << offset << '\n';

				if(tokens.get(++i).type != TK_ASSIGMENT) {
					error_exit("Assigment operator expected.");
				}

				while(tokens.get(++i).type != TK_EOS) {
					Token term = tokens.get(i);
					if(expression_term(term.type)) {
						expression.add(term);
					} else {
						error_exit("Invalid expression term");
					}
				}

				shunting_yard(expression);
				std::cout << "Expression: \n" << expression << std::endl;
				write_expression_to(assignee, expression, offset, 4);
				expression.index = 0;
				std::cout << "----------------------------------------\n\n";
			} break;

			case TK_LONG: {
				logln("Variable declaration ->");

				if(tokens.get(++i).type != TK_ID_LONG) {
					error_exit("Identifier expected.");
				}

				Token assignee = tokens.get(i);
				if(symbol_table.get(assignee.value) != -1) {
					error_exit("Identifier already used.");
				}
				else {
					offset += 8;
					symbol_table.add(assignee.value, offset);
				}
				std::cout << "Assignee: " << assignee.value << '\n';

				if(tokens.get(++i).type != TK_ASSIGMENT) {
					error_exit("Assigment operator expected.");
				}

				while(tokens.get(++i).type != TK_EOS) {
					Token term = tokens.get(i);
					if(expression_term(term.type)) {
						expression.add(term);
					} else {
						error_exit("Invalid expression term");
					}
				}

				shunting_yard(expression);
				std::cout << "Expression: \n" << expression << std::endl;
				write_expression_to(assignee, expression, offset, 8);
				expression.index = 0;
				std::cout << "----------------------------------------\n\n";
			} break;

			case TK_ID_INT: {
				logln("Identifier modification -> ");

				Token assignee = current;
				if(symbol_table.get(assignee.value) == -1) {
					error_exit("Variable not declared.");
				}
				std::cout << "Assignee: " << assignee.value << '\n';

				if(tokens.get(++i).type != TK_ASSIGMENT) {
					error_exit("Assigment operator expected.");
				}

				while(tokens.get(++i).type != TK_EOS) {
					Token term = tokens.get(i);
					if(expression_term(term.type)) {
						expression.add(term);
					} else {
						error_exit("Invalid expression term");
					}
				}

				shunting_yard(expression);
				std::cout << "Expression: \n" << expression << std::endl;
				write_expression_to(assignee, expression, offset, 4);
				expression.index = 0;
				std::cout << "----------------------------------------\n\n";
			} break;

			case TK_PRINT:
				logln("Output -> ");

				if(tokens.get(++i).type != TK_LPARENT) {
					error_exit("Left parenthesis expected.");
				}

				while(tokens.get(++i).type != TK_EOS) {
					Token token = tokens.get(i);

					switch(token.type) {
						case TK_NUMBER: {
							logln("Number:");

							short integer_expression = 0;
							while(token.type != TK_COMMA && token.type != TK_RPARENT) {
								if(token.type == TK_ID_INT) {
									if(integer_expression == 2) {
										error_exit("Long and int mixing is forbidden.");
									}
									else {
										integer_expression = 1;
									}
								}
								else if(token.type == TK_ID_LONG) {
									if(integer_expression == 1) {
										error_exit("Long and int mixing is forbidden.");
									}
									else {
										integer_expression = 2;
									}								
								}

								if(expression_term(token.type)) {
									expression.add(token);
								}
								else {
									error_exit("Invalid expression term. parser:213");
								}
								token = tokens.get(++i);
							}

							shunting_yard(expression);
							std::cout << "Expression: \n" << expression << '\n';
							if(integer_expression) {
								write_expression_to(TK_PLACEHOLD, expression, offset, 4);
							}
							else {
								write_expression_to(TK_PLACEHOLD, expression, offset, 8);
							}
							expression.index = 0;

							write_print_number();

						} break;

						case TK_ID_INT: {
							logln("Integer");
							while(token.type != TK_COMMA && token.type != TK_RPARENT) {
								if(expression_term(token.type) && token.type != TK_ID_LONG) {
									expression.add(token);
								}
								else {
									error_exit("Invalid expression term. parser:239");
								}
								token = tokens.get(++i);
							}

							shunting_yard(expression);
							std::cout << "Expression: \n" << expression << '\n';
							write_expression_to(TK_PLACEHOLD, expression, offset, 4);
							expression.index = 0;

							write_print_number();
						} break;

						case TK_ID_LONG: {
							logln("Long");
							while(token.type != TK_COMMA && token.type != TK_RPARENT) {
								if(expression_term(token.type) && token.type != TK_ID_INT) {
									expression.add(token);
								}
								else {
									error_exit("Invalid expression term");
								}
								token = tokens.get(++i);
							}

							shunting_yard(expression);
							std::cout << "Expression: \n" << expression << '\n';
							write_expression_to(TK_RAX, expression, offset, 8);
							expression.index = 0;

							write_print_number();
						} break;

						case TK_WORD: {
							unsigned char letters[8];
							std::cout << "Word: " << token.value << '\n';
								
							int letter_index = 0;
							int letter_offset = 0;
							int x = 0;
							for(x = 0; x < token.length; x++) {
								if(letter_index < 8) {
									letters[letter_index++] = token.value[x];
								}
								else {
									write_string_to_buffer(letter_offset, letters, 8);
									letter_offset += 8;
									letter_index = 0;
									x--;
								}
							}
							
							write_string_to_buffer(letter_offset, letters, letter_index);
							write_print_buffer(token.length);
						} break;

						case TK_COMMA:
							// Ignore commas.
						break;

						case TK_RPARENT:
							// Ignore right parenthesis.
						break;

						default:
							std::cout << "Token: " << token << '\n';
							error_exit("Invalid token at parser:302.");
						break;
					}
				}
				std::cout << "----------------------------------------\n\n";
			break;

			case TK_IF: {
				logln("If statement ->");

				if(tokens.get(++i).type != TK_LPARENT) {
					error_exit("Left parenthesis expected.");
				}

				Token comparator;
				current = tokens.get(++i);
				while(current.type != TK_RPARENT) {
					if(expression_term(current.type)) {
						expression.add(current);
					}
					else if(comparator(current.type)) {
						comparator = current;
						shunting_yard(expression);
						std::cout << "Expression: \n" << expression << '\n';
						write_expression_to(TK_EAX, expression, offset + 4, 4);
						expression.index = 0;
					}
					else {
						error_exit("Invalid expression term. @parser:329");
					}

					current = tokens.get(++i);
				}

				shunting_yard(expression);
				std::cout << "Expression: \n" << expression << '\n';
				write_expression_to(TK_EAX, expression, offset + 8, 4);
				expression.index = 0;

				write_if_statement(comparator, offset);

				if(tokens.get(++i).type != TK_LBRACKET) {
					error_exit("Left bracket expected.");
				}
				
				scope_stack.add(Scope(TK_IF, SIZE_OF_TEXT));
				std::cout << "----------------------------------------\n\n";
			} break;

			case TK_WHILE: {
				logln("While statement ->");

				if(tokens.get(++i).type != TK_LPARENT) {
					error_exit("Left parenthesis expected.");
				}

				Token comparator;
				Scope current_scope = Scope(TK_WHILE);
				current = tokens.get(++i);
				while(current.type != TK_RPARENT) {
					if(expression_term(current.type)) {
						expression.add(current);
					}
					else if(comparator(current.type)) {
						comparator = current;
						shunting_yard(expression);
						std::cout << "Expression: \n" << expression << '\n';
						write_expression_to(TK_EAX, expression, offset + 4, 4);
						current_scope.set_first_expression(expression);
						expression.index = 0;
					}
					else {
						error_exit("Invalid expression term parser:330");
					}

					current = tokens.get(++i);
				}

				shunting_yard(expression);
				std::cout << "Expression: \n" << expression << '\n';
				write_expression_to(TK_EAX, expression, offset + 8, 4);
				current_scope.set_second_expression(expression);
				expression.index = 0;

				current_scope.jump = SIZE_OF_TEXT;
				current_scope.comparator_type = comparator.type;
				write_while_statement(current_scope, offset, false);

				if(tokens.get(++i).type != TK_LBRACKET) {
					error_exit("Left bracket expected.");
				}

				scope_stack.add(current_scope);
				std::cout << "----------------------------------------\n\n";
			} break;

			case TK_ELSE: {
				logln("Else statement ->");
				Scope end_scope = scope_stack.pop();

				if(end_scope.type != TK_IF) {
					error_exit("Not in an If scope.");
				}

				Scope current_scope = Scope(TK_ELSE);
				current_scope.jump = SIZE_OF_TEXT;
				write_else_placeholder();
				write_end_if(end_scope);

				scope_stack.add(current_scope);
				std::cout << "----------------------------------------\n\n";
			} break;

			case TK_RBRACKET: {
				Scope end_scope = scope_stack.pop();

				switch(end_scope.type) {
					case TK_IF:
						write_end_if(end_scope);
					break;

					case TK_WHILE:
						write_expression_to(TK_EAX, end_scope.first_expression, offset + 4, 4);
						write_expression_to(TK_EAX, end_scope.second_expression, offset + 8, 4);
						write_while_statement(end_scope, offset, true);
						write_end_while(end_scope);
					break;

					case TK_ELSE:
						write_else_jump(end_scope);
					break;
				}
			} break;

			default:
				std::cout << "Default -> Token: " << current << '\n';
				error_exit("Invalid print statement.");
			break;
		}
	}
}