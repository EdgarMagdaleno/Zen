#include <iostream>

template <typename T>
struct List {
	T *list;
	int index;

	List() {
		index = 0;
	}

	List(int size) {
		index = 0;
		list = new T[size];
	}

	void add(T element) {
		list[index++] = element;
	}

	void add(T *element) {
		list[index++] = *element;
	}

	T &get(int index) {
		return list[index];
	}

	void set(int i, T element) {
		list[i] = element;
	}

	T pop() {
		return list[--index];
	}

	T top() {
		return list[index - 1];
	}

	bool empty() {
		return index == 0;
	}
};

template <typename T>
std::ostream& operator <<(std::ostream &output, const List<T> &list){
	for(int i = 0; i < list.index; i++) {
		output << "[" << i << "]\t" << list.list[i] << std::endl;
	}

	return output;
}