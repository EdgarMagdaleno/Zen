#include <cstring>

struct Token {
	int type;
	char *value;
	int length;
	long meta;

	Token() {
		type = -1;
		value = nullptr;
		meta = -1;
		length = -1;
	}

	Token(int _type, char *c) {
		type = _type;
		value = new char[1];
		strcpy(value, c);
		meta = -1;
		length = -1;
	}

	Token(int _type, List<char> &buffer) {
		type = _type;
		value = new char[buffer.index];
		strcpy(value, buffer.list);
		length = buffer.index;

		switch(_type) {
			case TK_NUMBER:
				meta = atol(buffer.list);
				if(buffer.get(99)) {
					meta *= -1;
					buffer.set(99, '\0');
				}
			break;

			default:
				meta = -1;
			break;
		}
	}

	Token(int _type) {
		type = _type;
		value = nullptr;
		length = -1;
		meta = -1;
	}
};

std::ostream& operator <<(std::ostream &output, const Token &token){
	output << "Type: " << token.type << "\t->";
	if(token.value != nullptr) output << "\tValue: " << token.value;
	return output;
}