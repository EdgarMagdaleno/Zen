section .text
	global _start

print_number:
	mov r10, r8
	add r10, 11
	mov r11, rax
	mov rdi, 0
	mov rcx, 10

	cmp rax, 0
	jl convert_negative
	jmp put_digit_on_buffer

convert_negative:
	mov rbx, -1
	mul rbx
	jmp put_digit_on_buffer

add_minus:
	sub r10, 1
	mov byte [r10], '-'
	inc rdi
	jmp print

put_digit_on_buffer:
	mov rdx, 0
	div rcx
	add rdx, 0x30
	sub r10, 1
	mov [r10], dl
	inc rdi
	cmp rax, 0
	jne put_digit_on_buffer

	cmp r11, 0
	jl add_minus
	
print:
	; Print
	mov rdx, rdi
	mov rax, 1
	mov rdi, 1
	mov rsi, r10
	syscall
	ret

_start:
	mov rbp, rsp
	mov r8, rbp

	mov rax, 45
	mov [rbp - 4], rax
	mov rax, 81
	mov [rbp - 8], rax

	mov rax, 1234567891084
	call print_number

	push rax
	push rax
	push rax

	; Exit
	mov rax, 60
	mov rdi, 0
	syscall