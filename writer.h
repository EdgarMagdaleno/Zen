#define		SIZE_OF_HEADER		432
#define		SCOPE_LIMIT			64
int 		SIZE_OF_FUNCTIONS 	= 0;
int 		SIZE_OF_TEXT 		= 0;

FILE *file;
hash_table symbol_table;
List<Scope> scope_stack(SCOPE_LIMIT);
char output_name[64];

void write_output_routines() {
	char routines[] = "\x4D\x89\xC2\x49\x83\xC2\x0B\x49\x89\xC3\xBF\x00\x00\x00\x00\xB9\x0A\x00\x00\x00\x48\x83\xF8\x00\x7C\x02\xEB\x19\x48\xC7\xC3\xFF\xFF\xFF\xFF\x48\xF7\xE3\xEB\x0D\x49\x83\xEA\x01\x41\xC6\x02\x2D\x48\xFF\xC7\xEB\x22\xBA\x00\x00\x00\x00\x48\xF7\xF1\x48\x83\xC2\x30\x49\x83\xEA\x01\x41\x88\x12\x48\xFF\xC7\x48\x83\xF8\x00\x75\xE4\x49\x83\xFB\x00\x7C\xD1\x48\x89\xFA\xB8\x01\x00\x00\x00\xBF\x01\x00\x00\x00\x4C\x89\xD6\x0F\x05\xC3";
	fwrite(&routines, sizeof(routines) - 1, 1, file);
	SIZE_OF_FUNCTIONS += sizeof(routines) - 1;
}

void write_if_statement(Token comparator, int offset) {
	std::cout << "Comparator: " << comparator << '\n';
	int temp_offset = offset * -1 - 4;
	fputs("\x8B\x85", file);
	fwrite(&temp_offset, sizeof(int), 1, file);
	temp_offset -= 4;

	fputs("\x3b\x85", file);
	fwrite(&temp_offset, sizeof(int), 1, file);

	SIZE_OF_TEXT += 12;
	switch(comparator.type) {
		case TK_LESSER: {
			char comparator_buffer[] = "\x0F\x8D\xFF\xFF\xFF\xFF";
			fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
		} break;

		case TK_GREATER: {
			char comparator_buffer[] = "\x0F\x8E\xFF\xFF\xFF\xFF";
			fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);	
		} break;

		case TK_LESSEREQ: {
			char comparator_buffer[] = "\x0F\x8F\xFF\xFF\xFF\xFF";
			fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
		} break;

		case TK_GREATEREQ: {
			char comparator_buffer[] = "\x0F\x8C\xFF\xFF\xFF\xFF";
			fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
		} break;

		case TK_SAME: {
			char comparator_buffer[] = "\x0F\x85\xFF\xFF\xFF\xFF";
			fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
		} break;

		case TK_DIFFERENT: {
			char comparator_buffer[] = "\x0F\x84\xFF\xFF\xFF\xFF";
			fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
		} break;

		default:
			error_exit("Invalid comparator type.");
		break;
	}

	SIZE_OF_TEXT += 6;
}

void write_end_if(Scope scope) {
	fclose(file);

	file = fopen(output_name, "r+");
	fseek(file, scope.jump + SIZE_OF_HEADER + SIZE_OF_FUNCTIONS - 4, SEEK_SET);
	int jump_offset = (scope.jump - SIZE_OF_TEXT) * -1;
	fwrite(&jump_offset, sizeof(int), 1, file);
	fclose(file);

	file = fopen(output_name, "a");
}

void write_else_placeholder() {
	char else_placeholder[] = "\xE9\x00\x00\x00\x00";
	fwrite(&else_placeholder, sizeof(else_placeholder) - 1, 1, file);
	SIZE_OF_TEXT += 5;
}

void write_else_jump(Scope scope) {
	fclose(file);

	file = fopen(output_name, "r+");
	fseek(file, scope.jump + SIZE_OF_HEADER + SIZE_OF_FUNCTIONS + 1, SEEK_SET);
	int jump_offset = (scope.jump - SIZE_OF_TEXT + 5) * -1;
	fwrite(&jump_offset, sizeof(int), 1, file);
	fclose(file);

	file = fopen(output_name, "a");
}

void write_while_statement(Scope scope, int offset, bool end) {
	int temp_offset = offset * -1 - 4;
	fputs("\x8B\x85", file);
	fwrite(&temp_offset, sizeof(int), 1, file);
	temp_offset -= 4;

	fputs("\x3b\x85", file);
	fwrite(&temp_offset, sizeof(int), 1, file);

	SIZE_OF_TEXT += 12;

	switch(scope.comparator_type) {
		case TK_LESSER: {
			if(!end) {
				char comparator_buffer[] = "\x0F\x8D\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
			else {
				char comparator_buffer[] = "\x0F\x8C\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
		} break;

		case TK_GREATER: {
			if(!end) {
				char comparator_buffer[] = "\x0F\x8E\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
			else {
				char comparator_buffer[] = "\x0F\x8F\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);				
			}
		} break;

		case TK_LESSEREQ: {
			if(!end) {
				char comparator_buffer[] = "\x0F\x8F\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
			else {
				char comparator_buffer[] = "\x0F\x8E\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);				
			}
		} break;

		case TK_GREATEREQ: {
			if(!end) {
				char comparator_buffer[] = "\x0F\x8C\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);				
			}
			else {
				char comparator_buffer[] = "\x0F\x8D\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);				
			}
		} break;

		case TK_SAME: {
			if(!end) {
				char comparator_buffer[] = "\x0F\x85\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
			else {
				char comparator_buffer[] = "\x0F\x84\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
		} break;

		case TK_DIFFERENT:
			if(!end) {
				char comparator_buffer[] = "\x0F\x84\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);
			}
			else {
				char comparator_buffer[] = "\x0F\x85\xFF\xFF\xFF\xFF";
				fwrite(&comparator_buffer, sizeof(comparator_buffer) - 1, 1, file);				
			}
		break;

		default:
			error_exit("Invalid comparator type writer:146.");
		break;
	}

	SIZE_OF_TEXT += 6;
}

void write_end_while(Scope scope) {
	fclose(file);

	file = fopen(output_name, "r+");
	fseek(file, scope.jump + SIZE_OF_HEADER + SIZE_OF_FUNCTIONS + 14, SEEK_SET);
	int jump_offset = (scope.jump + 18 - SIZE_OF_TEXT) * -1;
	fwrite(&jump_offset, sizeof(int), 1, file);

	fseek(file, SIZE_OF_TEXT + SIZE_OF_FUNCTIONS + SIZE_OF_HEADER - 4, SEEK_SET);
	jump_offset = (scope.jump - SIZE_OF_TEXT) + 18;
	fwrite(&jump_offset, sizeof(int), 1, file);

	fclose(file);

	file = fopen(output_name, "a");	
}

void write_print_buffer(int length) {
	char print_buffer[] = "\x48\xC7\xC0\x01\x00\x00\x00\x48\xC7\xC7\x01\x00\x00\x00\x4C\x89\xC6";
	fwrite(&print_buffer, sizeof(print_buffer) - 1, 1, file);
	SIZE_OF_TEXT += 17;
	if(length != 0) {
		fputs("\x48\xC7\xC2", file);
		fwrite(&length, sizeof(int), 1, file);
		SIZE_OF_TEXT += 7;
	}
	fputs("\x0F\x05", file);
	SIZE_OF_TEXT += 2;
}

void write_print_number() {
	char call_print_number[] = "\x48\xC7\xC3\xB0\x01\x40\x00\xFF\xD3";
	fwrite(&call_print_number, sizeof(call_print_number) - 1, 1, file);
	SIZE_OF_TEXT += 9;
}

void write_string_to_buffer(int offset, unsigned char letters[8], int size) {
	fputs("\x48\xB8", file);
	SIZE_OF_TEXT += 2;
	char null = '\x00';
	for(int i = 0; i < 8; i++) {
		if(i < size) {
			fwrite(&letters[i], sizeof(unsigned char), 1, file);
		}
		else {
			fwrite(&null, sizeof(unsigned char), 1, file);
		}
	}

	fputs("\x48\x89\x85", file);
	fwrite(&offset, sizeof(int), 1, file);
	SIZE_OF_TEXT += 15;
}

void write_expression_to(Token assignee, List<Token> &expression, int offset, int size) {
	fputs("\x49\x89\xE9\x48\x81\xED", file);
	fwrite(&offset, sizeof(int), 1 , file);
	SIZE_OF_TEXT += 10;

	int numbers;
	for(int i = 0; i < expression.index; i++) {
		Token current = expression.get(i);
		switch(current.type) {
			case TK_ID_LONG:
				fputs("\x49\x8B\x81", file);
				numbers = -symbol_table.get(current.value);
				fwrite(&numbers, sizeof(int), 1, file);
				fputs("\x50", file);
				SIZE_OF_TEXT += 8;
			break;

			case TK_ID_INT:
				fputs("\x41\x8B\x81", file);
				numbers = -symbol_table.get(current.value);
				fwrite(&numbers, sizeof(int), 1, file);
				fputs("\x50", file);
				SIZE_OF_TEXT += 8;
			break;

			case TK_NUMBER: {
				switch(size) {
					case 4:
						fputs("\xB8", file);
						numbers = current.meta;
						fwrite(&numbers, sizeof(int), 1, file);
						fputs("\x50", file);
						SIZE_OF_TEXT += 6;
					break;

					case 8: {
						fputs("\x48\xB8", file);
						long long_number = current.meta;
						fwrite(&long_number, sizeof(long), 1, file);
						fputs("\x50", file);
						SIZE_OF_TEXT += 11;
					} break;
				}
			} break;

			case TK_SUM:
				switch(size) {
					case 4:
						fputs("\x58\x5B\x01\xD8\x50", file);
						SIZE_OF_TEXT += 5;
					break;

					case 8:
						fputs("\x58\x5B\x48\x01\xD8\x50", file);
						SIZE_OF_TEXT += 6;
					break;
				}
			break;

			case TK_MULT:
				switch(size) {
					case 4:
						fputs("\x58\x5B\xF7\xE3\x50", file);
						SIZE_OF_TEXT += 5;
					break;

					case 8:
						fputs("\x58\x5B\x48\xF7\xE3\x50", file);
						SIZE_OF_TEXT += 6;
					break;
				}
			break;

			case TK_DIVIS:
				switch(size) {
					case 4: {
						char divis_buffer[] = "\xBA\x00\x00\x00\x00\x5B\x58\xF7\xF3\x50";
						fwrite(&divis_buffer, sizeof(divis_buffer) - 1, 1, file);
						SIZE_OF_TEXT += 10;
					} break;

					case 8: {
						char divis_buffer[] = "\x48\xC7\xC2\x00\x00\x00\x00\x5B\x58\x48\xF7\xF3\x50";
						fwrite(&divis_buffer, sizeof(divis_buffer) - 1, 1, file);
						SIZE_OF_TEXT += 13;
					} break;
				}
			break;

			case TK_MODULO:
				switch(size) {
					case 4: {
						char modulo_buffer[] = "\xBA\x00\x00\x00\x00\x5B\x58\xF7\xF3\x89\xD0\x50";
						fwrite(&modulo_buffer, sizeof(modulo_buffer) - 1, 1, file);
						SIZE_OF_TEXT += 12;
					} break;

					case 8: {
						char modulo_buffer[] = "\x48\xC7\xC2\x00\x00\x00\x00\x5B\x58\x48\xF7\xF3\x48\x89\xD0\x50";
						fwrite(&modulo_buffer, sizeof(modulo_buffer) - 1, 1, file);
						SIZE_OF_TEXT += 16;
					} break;
				}
			break;
		}
	}

	fputs("\x48\x81\xC5", file);
	fwrite(&offset, sizeof(int), 1 , file);
	SIZE_OF_TEXT += 7;

	switch(assignee.type) {
		case TK_ID_INT:
			fputs("\x89\x85", file);
			numbers = -symbol_table.get(assignee.value);
			fwrite(&numbers, sizeof(int), 1, file);
			SIZE_OF_TEXT += 6;
		break;

		case TK_ID_LONG:
			fputs("\x48\x89\x85", file);
			numbers = -symbol_table.get(assignee.value);
			fwrite(&numbers, sizeof(int), 1, file);
			SIZE_OF_TEXT += 7;
		break;

		case TK_PLACEHOLD:

		break;

		default:
			fputs("\x89\x85", file);
			numbers = -offset;
			fwrite(&numbers, sizeof(int), 1, file);
			SIZE_OF_TEXT += 6;
		break; 
	}
}

void write_exit() {
	char exit[] = "\x48\xC7\xC0\x3C\x00\x00\x00\x48\xC7\xC7\x01\x00\x00\x00\x0F\x05";
	fwrite(exit, 1, sizeof(exit) - 1, file);
	SIZE_OF_TEXT += 16;
	fclose(file);
}

void write_header() {
	file = fopen(output_name, "w");
	char header[] = "\x7F\x45\x4C\x46\x02\x01\x01\x00\x00\x00\x00\x00\x00\x5A\x65\x6E\x02\x00\x3E\x00\x01\x00\x00\x00\xE0\x01\x40\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40\x00\x38\x00\x01\x00\x40\x00\x03\x00\x01\x00\x01\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x0B\x02\x00\x00\x00\x00\x00\x00\x0B\x02\x00\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40\x01\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x18\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x70\x01\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x11\x00\x00\x00\x01\x00\x00\x00\x06\x00\x00\x00\x00\x00\x00\x00\x90\x01\x40\x00\x00\x00\x00\x00\x90\x01\x00\x00\x00\x00\x00\x00\x2B\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x17\x00\x00\x00\x10\x00\x01\x00\xBD\x01\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x2E\x73\x79\x6D\x74\x61\x62\x00\x2E\x73\x74\x72\x74\x61\x62\x00\x2E\x74\x65\x78\x74\x00\x5F\x73\x74\x61\x72\x74\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
	fwrite(header, 1, sizeof(header) - 1, file);
	fclose(file);
	file = fopen(output_name, "a");
}

void write_start_of_text() {
	fputs("\x48\x89\xE5\x49\x89\xE8", file);
	SIZE_OF_TEXT += 6;
}

void set_addresses() {
	FILE *file = fopen(output_name, "r+");
	unsigned int size_of_file = file_size(file);
	if(size_of_file != SIZE_OF_HEADER + SIZE_OF_FUNCTIONS + SIZE_OF_TEXT) {
		error_exit("Wrong file size.");
	}

	unsigned int start_of_program = 4194304 + SIZE_OF_FUNCTIONS + SIZE_OF_HEADER;
	fseek(file, 24, SEEK_SET);
	fwrite(&start_of_program, sizeof(unsigned int), 1, file);

	fseek(file, 96, SEEK_SET);
	fwrite(&size_of_file, sizeof(unsigned int), 1, file);

	fseek(file, 104, SEEK_SET);
	fwrite(&size_of_file, sizeof(unsigned int), 1, file);

	unsigned int size_of_text_section = size_of_file - SIZE_OF_HEADER + SIZE_OF_FUNCTIONS;
	fseek(file, 288, SEEK_SET);
	fwrite(&size_of_text_section, sizeof(unsigned int), 1, file);

	fclose(file);
}

void change_output_permissions() {
	const char *command = "chmod +x \"";
	char command_buffer[(sizeof(command)) + sizeof(output_name) - 1];
	strcpy(command_buffer, command);
	strcat(command_buffer, output_name);
	strcat(command_buffer, "\"");

	std::cout << "Attempting to give executable permissions to output:\n" << command_buffer << '\n';
	int response = system(command_buffer);
	if(response == 0) {
		std::cout << "Permissions set succesfully.\n";
	}
	else {
		std::cout << "An error was produced while attemting to set permissions, exit code: " << response << '\n';
	}

}