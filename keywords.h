#define		KEYWORDS_SIZE	8

char RESERVED_KEYWORDS[KEYWORDS_SIZE][10] = {
	"int",
	"function",
	"while",
	"if",
	"string",
	"long",
	"print",
	"else"
};

short TOKEN_EQUIVALENCE[KEYWORDS_SIZE] {
	TK_INT,
	TK_FUNCTION,
	TK_WHILE,
	TK_IF,
	TK_STRING,
	TK_LONG,
	TK_PRINT,
	TK_ELSE
};